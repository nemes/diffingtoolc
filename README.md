# DiffingToolC

## Description

This is a tool for analyzing C structs and diffing them semantically.

When simply analyzing, it will a JSON structure per structure that contains the following info:
* struct name
* struct fields
* for each individual field it will contain information such as:
* * type
* * is it a pointer
* * is it an array
* * array size
* * ifdefs associated with this field

If used for diffing it will output differences such as:
* field removal
* field addition
* field change (change of type, change of size)
* field position change
* changes in ifdefs

## Installation instructions

* Tested on python 3.8.10
* Optionally create a virtual environment
* pip install -r requirements.txt

## How to run

### Analysis mode

Analysis mode is the mode used to generate the results for the paper. It takes the result folder of the previous phase of extracting all structs per kernel version and outputs similarly structured files but with parsed structs instead. It does no diffing, only parsing of all of the extracted structs.

To run struct large-scale struct analysis simply run:

```script
python analyze.py -i <path_to_input_folder_containing_previous_results> -o <path_to_empty_folder_for_new_results>
```


### Diff mode 

Diff mode can be used to diff to structs that are extracted in two separate files, 2 positional arguments are required, filepaths to the left and right struct.

There are example files within the repository, to quickly try out the application, simply run the following command from the root directory:
```script
python diff.py examples/basic/left.c examples/basic/right.c
```
Since no output mode was set, the program executed without outputing the results.

#### Output to terminal

```script
python diff.py examples/basic/left.c examples/basic/right.c --terminal
```

#### Output to file

If you wish for the results to be outputed to a file, use the --output flag

```script
python diff.py examples/basic/left.c examples/basic/right.c --output test.txt
```

#### Diff according to field position as well

If you want the diffing algorithm to take into acount field position changes, add the **-p** flag like so:

```script
python diff.py examples/basic/left.c examples/basic/right.c --terminal -p
```

#### Output information about individual structs along with diff results

If you wish to see how the individual structs were parsed, you can use the **-s** flag like so:

```script
python diff.py examples/basic/left.c examples/basic/right.c --terminal -s
```

#### Output verbose struct information about individual structs along with diff results

If you wish to see even more information about individual structs, you can use the **-v** flag in conjuction with **-s** like so:

```script
python diff.py examples/basic/left.c examples/basic/right.c --terminal -s -v
```

For further instructions on argument usage, run:
```script
python diff.py --help
```


## How to run tests

* It's possible to run tests by executing the following command in the root directory:
```script
python -m unittest
```
* To make test output more verbose, use -v flag
* To run a specific test, use -k *test_name*


## In case of crash during parsing

The parser doesn't fully capture the syntax of C, especially since it attempts to parse preprocessing directives at the same time. These would be fully handled by the C preprocessor in case of proper compilation.
Due to this, it can fail on certain kernel code.
However it attempts to help the user adjust the code by pointing to the specific line that caused the error and the missing token.
Sometimes the error is actually in the line before or after, so it prints a snippet of code surrounding the affected line.